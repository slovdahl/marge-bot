FROM python:3.12-slim AS builder

ARG POETRY_VERSION=2.0.1
RUN pip install poetry==$POETRY_VERSION

WORKDIR /src

COPY pyproject.toml poetry.lock README.md pylintrc ./
COPY marge/ ./marge/
RUN poetry self add poetry-plugin-export && \
  poetry export -o requirements.txt && \
  poetry build


FROM python:3.12-slim

RUN apt-get update && apt-get install -y \
  git-core \
  && \
  rm -rf /var/lib/apt/lists/* && \
  adduser --system marge-bot

COPY --from=builder /src/requirements.txt /src/dist/marge_bot-*.tar.gz /tmp/

RUN pip install --no-deps -r /tmp/requirements.txt && \
  pip install /tmp/marge_bot*.tar.gz

USER marge-bot

ENTRYPOINT ["marge"]
