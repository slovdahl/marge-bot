# Contributing to marge-bot

marge-bot is a community project and contributions are more than welcome to keep the project going.
Below are some basic guidelines to help you get started.

## Commit messages

We use the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification to format all our commit messages.
As a community-maintained project without a dedicated release management team, this helps us automate our releases
and changelog generation.

Commit formats are enforced using [commitizen](https://commitizen-tools.github.io/commitizen/) in our merge request pipelines. Please make sure your commit messages match this format and [amend your commit message](https://docs.github.com/en/pull-requests/committing-changes-to-your-project/creating-and-editing-commits/changing-a-commit-message) if the
commit lint step fails.

Below are a few examples of commits that would fail or pass our checks:

* Bad: `Added support for batching`
* Good: `feat(api): add support for batching merge requests`
* Bad: `Update documentation for batch jobs`
* Good: `docs(projects): update example for batch jobs`
